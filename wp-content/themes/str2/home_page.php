<?php
/*
	Template Name: Home Page
*/
?>

<?php get_header(); ?>

		<div class='green-bar'></div>
		<div class='white-bar'></div>

<?php while (have_posts()) { the_post(); ?> 

	<?php
		
		$slide1Title = get_field('slide_1_title');
		$slide1PostId = get_field('slide_1_post_id');
		$slide1Image = get_field('slide_1_image');

		$slide2Title = get_field('slide_2_title');
		$slide2PostId = get_field('slide_2_post_id');
		$slide2Image = get_field('slide_2_image');

		$slide3Title = get_field('slide_3_title');
		$slide3PostId = get_field('slide_3_post_id');
		$slide3Image = get_field('slide_3_image');

	?>

	<!--<div class='flexslider'>
		<ul class='slides'>
		
			<li>
				<img src='<?php echo $slide1Image; ?>' />
				<h1>
					<a href='<?php echo get_permalink($slide1PostId); ?>'><?php echo $slide1Title; ?></a>
				</h1>
			</li>

			<li>
				<img src='<?php echo $slide2Image; ?>' />
				<h1>
					<a href='<?php echo get_permalink($slide2PostId); ?>'><?php echo $slide2Title; ?></a>
				</h1>
			</li>

			<li>
				<img src='<?php echo $slide3Image; ?>' />
				<h1>
					<a href='<?php echo get_permalink($slide3PostId); ?>'><?php echo $slide3Title; ?></a>
				</h1>
			</li>

		</ul>
	</div>-->

	<section id='content'>

		<div class='thank-you'>
			<h2>Thank you racers!</h2>
			<p>
				Thank you for another successful season of racing in WNY!  You are what makes putting on these races so fun and satisfying.
			</p>

			<p>
				Huge thanks to all the sponsors this year, check them out on our sponsors page.  Specifically we'd like to recognize <a href='http://campuswheelworks.com/' target='_blank'>Campus Wheelworks</a> - Ethan and Alex and a whole crew of volunteers.  They really made this year's races 
				organized, fun, safe, and accessible to all.  Without their help these races may not have happened.
			</p>

			<p>
				Thank you Thom Wright (and crew) for all you do making great trails available in Chautauqua Co. and for making the Hustle an outstanding event in all regards.
			</p>

			<p>
				We'd also like to recognize <a href='http://wnymba.org/' target='_blank'>WNYMBA</a>, our local IMBA chapter who helps build, maintain, and advocate for local trail access.  Without them we wouldn't have a place
				to ride, race, and challenge ourselves in the woods.
			</p>

			<p>
				Stay tuned here for updates on the 2016 schedule.  Happy trails.
			</p>

			<img src='<?php echo get_stylesheet_directory_uri(); ?>/images/str_plates.png' title='2015 Race Plates' />
		</div>

	</section>

<?php } ?>

<?php get_footer(); ?>