<?php
/*
	Template Name: Sponsor Page
*/
?>

<?php get_header(); ?>
  
<?php
	$pageTitle = get_the_title();
	include 'header_details.php';
?>

<div class='page-content'>

	<?php
		$showSpringLinks = (get_the_id() == 54);  // only show links on spring series page
   		$showNewsletter = (get_the_id() == 4);  // contact us, show newsletter 
		include 'sidebar.php'; 
	?>

		<section class='details'>
		
			<?php
				query_posts('cat=7');  /* 7 = sponsors */

				while (have_posts()): 
					the_post();
					$sponsor_name = get_field('name');
					$sponsor_url = get_field('website_url'); 
			?>
				
				<a href='<?php echo $sponsor_url; ?>' target='_blank'><img src='<?php the_field('logo'); ?>' class='sponsor-logo' alt='<?php echo $sponsor_name; ?>' title='<?php echo $sponsor_name; ?>' /></a>

			<?php
				endwhile;

				wp_reset_query(); 
			?>

		</section>

</div>

<?php get_footer(); ?>