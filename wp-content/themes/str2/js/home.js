var Home = (function($) {

	return {
		Load: function() {

			$('.flexslider').flexslider({
		    	animation: "slide",
		    	controlNav: false,
		    	directionNav: true,
		    	smoothHeight: false
			});

		}
	};

})(jQuery);

jQuery(document).ready(function() {

	Home.Load();

});