Page = {

	SlickNav: {

		Load: function() {

			var $nav = jQuery('.sidr-nav .menu-footer-container').slicknav();

			jQuery('#responsive-menu-button').click(function () {
				$nav.slicknav('toggle');
			});

		}
	},

	SideBar: {

		Load: function() {

			jQuery('.link-button').click(function () {
				var url = jQuery(this).attr('data-href');
				window.location.href = url;
			});

		}

	}

};

jQuery(document).ready(function() {

	Page.SlickNav.Load();
	Page.SideBar.Load();

});