var Event = (function ($) {

	return {

		CourseMap: {
			Load: function(gpxUrl) {

				var mapOptions = {
					zoom: 12,
					mapTypeId: google.maps.MapTypeId.TERRAIN
				}

				map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

				$.ajax({
					type: 'GET',
					url: gpxUrl,
					dataType: 'xml',
					success: function(xml) {
						var points = [];
						var bounds = new google.maps.LatLngBounds();

						$(xml).find('trkpt').each(function() {
							var lat = $(this).attr('lat');
							var lon = $(this).attr('lon');

							var p = new google.maps.LatLng(lat, lon);

							points.push(p);
							bounds.extend(p);
						});

						var poly = new google.maps.Polyline({
							path: points,
							strokeColor: '#3803BF',
							strokeOpacity: .7,
							strokeWeight: 3
						});

						poly.setMap(map);
						
						map.fitBounds(bounds);  // fit bounds to track
					}
				});

			}
		},

		ElevationGraph: {

			Url: '',

			Load: function(url) {

				Event.ElevationGraph.Url = url;

				google.load('visualization', '1.0', {
					'packages': ['corechart'],
					'callback': Event.ElevationGraph.DrawChart
				});

			},

			DrawChart: function(url) {

				var options = {
					backgroundColor: { stroke: '#000', strokeWidth: 2, fill: '#eaeaea' },
					colors: ['#26913C'],
					title: 'Elevation Profile',
					titleTextStyle: { fontSize: 12 },
					//legend: { position: 'none' },
					tooltip: { showColorCode: true },
					hAxis: { title: 'Distance (mi)', titleTextStyle: { fontSize: 10 } },
					vAxis: { title: 'Elevation (ft)', titleTextStyle: { fontSize: 10 },  },
					pointSize: 2,
					areaOpacity:  0.3
				};

				$.ajax({
					url: Event.ElevationGraph.Url,
					dataType: 'json',
					context: document.getElementById('chart'),
					success: function (serverData) {
						data = new google.visualization.DataTable(serverData);
						chart = new google.visualization.AreaChart(this);
						chart.draw(data, options);
					},
					error: function(data) {
						//var z = 0;
					}
				});

			}

		}
	};

})(jQuery);