<?php

$theme_minified_files = false;  // borrowed from justintallant.com

add_action('wp_enqueue_scripts', 'theme_scripts');
add_action('init', 'register_menus');

add_filter('stylesheet_uri', 'theme_stylesheet', 10, 2);
add_filter('upload_mimes', 'custom_upload_types');

function theme_scripts() {
	global $theme_minified_files;

	wp_enqueue_style('rong_font', 'http://fonts.googleapis.com/css?family=Architects+Daughter');

	$ext = ($theme_minified_files == true ? '.min.js' : '.js');

	wp_enqueue_script('slicknav', get_template_directory_uri() .'/js/jquery.slicknav'. $ext, array(), null, true);
	wp_enqueue_script('page', get_template_directory_uri() .'/js/page'. $ext, array(), null, true);

	if (is_page_template('event_page.php')) {
		wp_enqueue_script('events', get_template_directory_uri() .'/js/event-details'. $ext, array('jquery'), null, true);
	}

	if (is_page_template('home_page.php')) {
		wp_enqueue_script('flexslider', get_template_directory_uri() .'/js/flexslider/jquery.flexslider'. $ext, array(), null, true);
		wp_enqueue_script('home', get_template_directory_uri() .'/js/home'. $ext, array(), null, true);
	}

}

function register_menus() {
	register_nav_menus(
		array('tophat' => __('tophat'), 'main' => __('main'), 'footer' => __('footer'))
	);
}

function theme_stylesheet($stylesheet_uri, $stylesheet_dir_uri) {
	global $theme_minified_files;

	if ($theme_minified_files == true) {
		$stylesheet_uri = $stylesheet_dir_uri ."/style.min.css";
	}

	return $stylesheet_uri ."?v=". filemtime(get_stylesheet_directory() ."/style.css");
}

function custom_upload_types($mimes) {
	$mimes = array_merge($mimes, array('xml' => 'application/xml'));  // allow more file types to be uploaded
	$mimes = array_merge($mimes, array('gpx' => 'application/xml'));
	$mimes = array_merge($mimes, array('json' => 'application/json'));

    return $mimes;
}

function getDateString($date) {

	$y = substr($date, 0, 4);
	$m = substr($date, 4, 2);
	$d = substr($date, 6, 2);
	 
	// create UNIX
	$time = strtotime("{$d}-{$m}-{$y}");

	return date('m/d/Y', $time);

}

?>