<?php
	query_posts('cat='. $categoryId);

	if (have_posts())
	{
		while (have_posts())
		{
			the_post();
			the_content();
		}
	}

	wp_reset_query();
?>