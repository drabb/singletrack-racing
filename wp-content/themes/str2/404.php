<?php get_header(); ?>

<?php
	$pageTitle = get_the_title();
	include 'header_details.php';
?>

<div class='page-content'>

	
		<?php 
			include 'sidebar.php'; 
		?>

		<section class='details'>
		
			<div class='fourohfour'>
			<h1>404</h1>

			<p>
				Whoops-a-doozy!<br /><br />Well this is certainly embarrassing, but it looks like the page you was'a wanting don't exist.
			</p>

			<img src='<?php echo get_template_directory_uri(); ?>/images/404_shot.jpg' alt='404 Podium' />
			</div>
		</section>
	
</div>

<?php get_footer(); ?>