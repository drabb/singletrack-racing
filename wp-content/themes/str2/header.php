<!DOCTYPE html>
<html>
	<head <?php language_attributes(); ?>>
		<meta charset='<?php bloginfo('charset'); ?>' />
		<meta name='viewport' content='width=device-width' />

		<title><?php bloginfo('name'); ?> <?php wp_title("-"); ?></title>

		<link rel='shortcut icon' href='<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico' />
		
		<link rel='stylesheet' type='text/css' href='<?php bloginfo('stylesheet_directory'); ?>/css/slicknav.css' />

		<link rel='stylesheet' type='text/css' href='<?php bloginfo('stylesheet_url'); ?>' />
		<link rel='stylesheet' type='text/css' href='<?php bloginfo('stylesheet_directory'); ?>/js/flexslider/flexslider.css' />
		
		<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css' />

		<?php wp_head(); ?>

		<script src='http://connect.facebook.net/en_US/all.js#xfbml=1&appId=245024305664697'></script>

		<base href="<?php bloginfo('stylesheet_directory'); ?>/" />
	</head>
	<body>

		<div id='fb-root'></div>

		<header>

			<div class='top-hat-content'>
				<?php wp_nav_menu(array('theme_location' => 'tophat', 'menu_class' => 'top-nav-menu', 'after' => '<span>|</span>')); ?>
			</div>

			<a href='<?php echo esc_url(home_url('/')); ?>'>
				<img src='<?php echo get_template_directory_uri(); ?>/images/str_logo.png' class='logo' title='Singletrack Racing' alt='Singletrack Racing' />
			</a>

			<?php wp_nav_menu(array('theme_location' => 'main', 'menu_class' => 'nav-menu')); ?>
		</header>

		<div id='content'>
