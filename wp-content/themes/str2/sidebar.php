
<div class='sidebar'>

	<?php if ($showSpringLinks) { ?>
		<div class='sidebar-info'>

			<?php
				$categoryId = 9;  /* 8 = spring series links */
				include 'category_posts.php';
			?>

		</div>
	<?php } ?>

	<?php if ($showNewsletter) { ?>
		<div class='sidebar-info'>
			<?php
				$categoryId = 10;  /* 10 = mailchimp newsletter */
				include 'category_posts.php';
			?>
		</div>
	<?php } ?>

	<?php if (!$showNewsletter) { ?>
		<div class='sidebar-info contact-us'>

			<?php
				$categoryId = 6;  /* 6 = contact us content */
				include 'category_posts.php';
			?>

			<button data-href='<?php echo get_permalink(4); ?>' class='link-button'>
				Contact Us
			</button>
		</div>
	<?php } ?>

</div>