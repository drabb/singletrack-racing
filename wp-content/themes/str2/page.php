<?php get_header(); ?>

<?php
	$pageTitle = get_the_title();
	include 'header_details.php';
?>

<div class='page-content'>

	<?php while (have_posts()) { the_post(); ?>

		<?php 
			$showSpringLinks = (get_the_id() == 54);  // only show links on spring series page
			$showNewsletter = (get_the_id() == 4);  // contact us, show newsletter
			include 'sidebar.php'; 
		?>

		<section class='details'>
		
			<?php the_content('Read More'); ?> 

		</section>

	<?php } ?>
	
</div>

<?php get_footer(); ?>