
<?php get_header(); ?>
  
<?php while (have_posts()) { the_post(); ?> 

	<h2><?php the_title(); ?></h2>
	<hr class='title-rule' />

	<?php if ( is_archive() || is_search() ) : // Only display excerpts for archives and search. ?>  
            <?php the_excerpt(); ?>  
    <?php else : ?>  
            <?php the_content('Read More'); ?>  
    <?php endif; ?>  

<?php } ?>

<?php get_footer(); ?>
