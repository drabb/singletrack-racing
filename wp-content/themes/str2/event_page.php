<?php
/*
	Template Name: Event Page
*/
?>

<?php
	global $gpxFile, $jsonFile;
	get_header(); 
?>

<div class='event-content-wrapper'>

	<?php while (have_posts()): 

		the_post();

		$name = get_field('name');
		$locationName = get_field('location');
		$locationLink = get_field('location_link');
		$fbEventLink = get_field('facebook_event_link');

		$location = ($locationLink !== '' ? "<a href='${locationLink}' target='_blank' title='Map event location'>${locationName}</a>" : $locationName);

		$registrationLink = get_field('registration_link');
		$description = get_field('description');

		$dateLabel = getDateString(get_field('date'));

		$gpxFile = get_field('course_gpx');
		$elevationJson = get_field('elevation_json');

		$pageTitle = $name;
		include 'header_details.php';
	?>

		<div class='event-content'>
			<div class='registration-content'>

				<?php if ($registrationLink !== '') { ?>
					<div class='grey-wrapper registration-link'>
						<?php echo $registrationLink; ?>
					</div>
				<?php } ?>

				<?php if (get_field('logo') !== '') { ?>
					<div class='grey-wrapper logo'>
						<img src='<?php the_field('logo'); ?>' class='event-logo' alt='<?php echo $name; ?>' title='<?php echo $name; ?>' />
					</div>
				<?php } ?>

				<?php if ($fbEventLink !== '') { ?>
					<div class='grey-wrapper event'>
						<a href='<?= $fbEventLink; ?>' target='_blank'>
							<!-- View this event on Facebook -->
							<img src='<?= get_template_directory_uri(); ?>/images/facebook-event.gif' class='fb-event' alt='View event on Facebook' />
						</a>
					</div>
				<?php } ?>

				<?php include 'facebook_box.php'; ?>

			</div>

			<div class='description-content'>
				<div class='details'>
					<span><img src='<?php echo get_stylesheet_directory_uri(); ?>/images/map-pin.svg' class='pin' alt='Map pin' /><?php echo $location; ?></span>&nbsp;|&nbsp;<span><?php echo $dateLabel; ?></span>
				</div>

				<?php the_content('Read More'); ?>
			</div>
		</div>

		<div style='clear:both;'></div>

		<?php if ($gpxFile != '' && $elevationJson != '') { ?>

			<div class='course-details'>
				<div class='course-details-content'>
					<h3>Course Details</h3>
				</div>

				<div class='details-wrapper'>

					<div class='course-details-content'>
						
						<?php if ($gpxFile != '') { ?>
							<div class='course-map'>
								<h3>Course</h3>
								<div id='map_canvas' style='height:223px; border:solid 1px #000;'></div>
								<a href='<?php echo $gpxFile; ?>' target='_blank' download='course_details.gpx' class='download-link'>Download this course</a> as a GPX file.
							</div>
						<?php } ?>

						<?php if ($elevationJson != '') { ?>
							<div class='course-profile'>
								<h3>Profile</h3>
								<div id='chart' style='height:225px;'></div>
							</div>
						<?php } ?>

					</div>

				</div>
			</div>

		<?php } /* course details */ ?>
		
	<?php endwhile; ?>

</div>

<?php get_footer(); ?>

<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<script type="text/javascript" src='http://maps.googleapis.com/maps/api/js?key=AIzaSyAcFA712I4NbC1comG277Uid3DpZTSzzyw&sensor=false'></script>
<script>

jQuery(document).ready(function() {

	Event.CourseMap.Load('<?php echo $gpxFile; ?>');
	Event.ElevationGraph.Load('<?php echo $elevationJson; ?>');

});

</script>