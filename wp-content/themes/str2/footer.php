
		</div>

		<footer>
			<div class='white-bar'></div>
			<div class='green-bar'></div>
			<div class='content'>
				
				<div class='nav'>
					<?php wp_nav_menu(array('theme_location' => 'footer', 'menu_class' => 'nav-menu', 'after' => '<span class=\'separator\'>|</span>')); ?>
				</div>

				<div class='sidr-nav'>
					<?php wp_nav_menu(array('theme_location' => 'footer', 'menu_class' => 'sidr-nav-menu')); ?>
				</div>

				<div class='social-links'>
					<a href='http://www.facebook.com/singletrackracing' class='fb' target='_blank'></a>
				</div>

				<div class='credits'>
					All photography herein &copy; <a href='http://rongrucela.zenfolio.com/' target='_blank'>Ron Grucela</a>.
					<br />
					Single Track Racing <?php echo date("Y"); ?>.  All rights Reserved.
				</div>
			</div>
			
		</footer>
		<?php wp_footer(); ?>
	</body>
</html>