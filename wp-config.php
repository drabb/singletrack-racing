<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'singletrackracing');

/** MySQL database username */
define('DB_USER', 'str_wp');

/** MySQL database password */
define('DB_PASSWORD', 'slapChop1234');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 
define('AUTH_KEY',         'put your unique phrase here');
define('SECURE_AUTH_KEY',  'put your unique phrase here');
define('LOGGED_IN_KEY',    'put your unique phrase here');
define('NONCE_KEY',        'put your unique phrase here');
define('AUTH_SALT',        'put your unique phrase here');
define('SECURE_AUTH_SALT', 'put your unique phrase here');
define('LOGGED_IN_SALT',   'put your unique phrase here');
define('NONCE_SALT',       'put your unique phrase here');
*/

define('AUTH_KEY',         'T+moNFN}N+aJ#uSHA[M^}_is>trRcIx+IZSDH` v;x0_&8vGn%K*:d&}+IW<zgWs');
define('SECURE_AUTH_KEY',  '3>S;[q54 6)vJ*4ff4: R4<k6ww.(Y`54+[_)RlFO$a7Vgdy;HmGg8z^ld{G)+[X');
define('LOGGED_IN_KEY',    '|vnEuBG QXU81<xga7}V#-uGx#t-5F@i$`{h.:blIo=-zlpiV[T-8S-!rz<nfKW-');
define('NONCE_KEY',        '69j!y3=zX:igM&VBdY+1Nt~3.D}|x0^_U?!kG2?)Br-w}2+th?p[-7N]9cAE>j6x');
define('AUTH_SALT',        ':2j];=314*(#QF7vz]8rLf=pWf?^//aJ/^K!!9gu-/&~rU A9Z3]u-tqv6-,4.V{');
define('SECURE_AUTH_SALT', '<YO3+XGHvF|5.4?& ?+$+l-b!llQ.eb3FFh;T-DYarT&AgE+#7`(!56kQ~NbCs8e');
define('LOGGED_IN_SALT',   'x+a&`P<Zrs4^<>%s@rCE>4:tR80|9+-xQ>X(Qq|[R;#8+d-d6xn-]NJKe2@-_:+8');
define('NONCE_SALT',       'I2E|.1k6k4U>ZtJ1G`h2eevtba]}^^)s(_Kx{:_GS:`-@>^!|&:c+Y,6tJD=bjxA');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * Self explanatory, but used for building URLs.  Try to make deployments
 * and production restores less painful.
 */
define('WP_HOME', 'http://localhost/singletrackracing');
define('WP_SITEURL', 'http://localhost/singletrackracing');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/*set_time_limit(120);*/

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
